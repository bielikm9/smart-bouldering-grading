# Select whether training on a clean or pretrained model OR loading a model already trained on our dataset
# NOTE: Weights as well as additional datasets are omitted for space-saving
#       Only the best model and the dataset used to train it are present
models = [
    'clean',                                                # 0
    'pretrained',                                           # 1
    'model/weights/clean_climbing_242e.pt',                 # 2
    'model/weights/clean_climbing_hold_278e.pt',            # 3
    'model/weights/clean_hold_color_300e.pt',               # 4
    'model/weights/clean_hold_color_anno_300e.pt',          # 5
    'model/weights/pretrained_climbing_45e.pt',             # 6
    'model/weights/pretrained_climbing_hold_300e.pt',       # 7
    'model/weights/pretrained_comb_454e.pt',                # 8 Best
    'model/weights/pretrained_hold_color_68e.pt',           # 9
    'model/weights/pretrained_hold_color_anno_128e.pt',     # 10
]
init_model = models[8]

# Training params
datasets = [
    'data_climbing.yaml',           # 0 Rectangular
    'data_climbing_hold.yaml',      # 1
    'data_hold_color.yaml',         # 2
    'data_hold_color_anno.yaml',    # 3
    'data_comb.yaml',               # 4
]
train = {
    'dataset': datasets[4],
    'epochs': 900,
    'batch': 16,
    'imgsz': 640,
    'optimizer': 'auto',
    'seed': 0,
    'is_rect': False,
}

# Validation params
val = {
    'dataset': datasets[4],
    'save_json': True,
    'save_hybrid': True,
    'split': 'val',
    'run_validation': True,
}

# Prediction params
predict = {
    'source': 'dataset/predict',
    'save': True,
    'save_txt': True,
    'save_conf': True,
    'line_width': 2,
}

class_list = ['black', 'blue', 'gray', 'green', 'human', 'orange', 'pink', 'purple', 'red', 'white', 'yellow']
