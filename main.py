# 1. Export to openvino

# Imports
from ultralytics import YOLO
from ultralytics import settings
import pandas
import math
import params


# Functions
def update_settings():
    # Update dataset directory so we can avoid using absolute path
    settings.update({'datasets_dir': 'dataset'})


def load_base_model():
    # Select either clean, pretrained or trained model based on params
    match params.init_model:
        case 'clean':
            model = YOLO('yolov8s.yaml')
        case 'pretrained':
            model = YOLO('model/yolov8s.pt')
        case _:
            model = YOLO(params.init_model)
    return model


def is_training_new_model():
    return params.init_model == 'clean' or params.init_model == 'pretrained'


def train_model(model):
    # If using clean or pretrained model, train on our dataset
    # If an already trained model was selected, skip training
    if is_training_new_model():
        model.train(
            data=params.train['dataset'],
            epochs=params.train['epochs'],
            batch=params.train['batch'],
            imgsz=params.train['imgsz'],
            optimizer=params.train['optimizer'],
            seed=params.train['seed'],
            rect=params.train['is_rect'],
        )


def validate_model(model):
    if params.val['run_validation']:
        model.val(
            data=params.val['dataset'],
            save_json=params.val['save_json'],
            save_hybrid=params.val['save_hybrid'],
            split=params.val['split']
        )


def predict(model):
    return model.predict(
        source=params.predict['source'],
        save=params.predict['save'],
        save_txt=params.predict['save_txt'],
        save_conf=params.predict['save_conf'],
        line_width=params.predict['line_width'],
    )


def order_holds_by_height(holds):
    # Order is reversed, because max height is at the bottom in screen space coordinates
    # Y = 1 is the lowest possible hold in the picture (using normalised coordinates)
    for color in holds.keys():
        holds[color].sort(reverse=True, key=lambda x: x[1])

    return holds


def get_holds_per_class_from_result(result):
    holds = {}

    for cls, midpoint in zip(result.boxes.cls, result.boxes.xywhn[:, :2]):
        cls = params.class_list[int(cls)]
        if cls in holds.keys():
            holds[cls].append(midpoint)
        else:
            holds[cls] = [midpoint]

    return order_holds_by_height(holds)


def get_holds_from_results(results):
    result_holds = []

    for result in results:
        result = result.cpu()
        result = result.numpy()
        result_holds.append(get_holds_per_class_from_result(result))

    return result_holds


def angle_of_line(horizontal_change, vertical_change):
    # Angle is relative to a vector at the baseline of the original hold aiming towards the X coordinate of the
    # following hold
    return math.degrees(math.atan2(vertical_change, horizontal_change))


def measure_step_between_holds(data, start_hold, end_hold):
    # Measuring the Euclidean distance in screen space
    distance = math.dist(start_hold, end_hold)
    # Measuring the horizontal change in distance in screen space
    # Negative value => leftward motion, positive value => rightward motion
    horizontal_change = end_hold[0] - start_hold[0]
    # Measuring the vertical change in distance in screen space
    # Absolute value, because max. possible distance is at the bottom of screen space coordinates
    vertical_change = math.fabs(end_hold[1] - start_hold[1])
    # Measuring the angular change in motion
    angular_change = angle_of_line(math.fabs(horizontal_change), vertical_change)

    data['Midpoint'].append(end_hold)
    data['Distance'].append(round(distance, 4))
    data['Hor. change'].append(round(horizontal_change, 4))
    data['Vert. change'].append(round(vertical_change, 4))
    data['Ang. change'].append(round(angular_change, 4))


def create_measurement_dataframe(hold_list):
    # Starting hold has no measurements
    data = {
        'Midpoint': [hold_list[0]],
        'Distance': ['---'],
        'Hor. change': ['---'],
        'Vert. change': ['---'],
        'Ang. change': ['---']
    }

    for i in range(len(hold_list) - 1):
        measure_step_between_holds(data, hold_list[i], hold_list[i + 1])

    return pandas.DataFrame(data)


def get_measurements_for_singular_result(hold_result):
    measurements = {}

    for color in hold_result.keys():
        measurements[color] = create_measurement_dataframe(hold_result.get(color))

    return measurements


def get_measurements_from_hold_results(hold_results):
    measurement_results = []

    for result in hold_results:
        measurement_results.append(get_measurements_for_singular_result(result))

    return measurement_results


def main():
    update_settings()

    model = load_base_model()

    train_model(model)
    validate_model(model)

    results = predict(model)
    hold_results = get_holds_from_results(results)
    measurement_results = get_measurements_from_hold_results(hold_results)

    print(measurement_results[0]['green'].to_latex())


# Script
if __name__ == '__main__':
    main()
